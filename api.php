<?php

/* File : api.php
* Api method
* Author : Amir ( Techie Lab Enterprise )
* Last Modify : 22 / 09 / 2018
*/
     
require_once("Rest.inc.php");
     
class API extends REST {
     
    public $data = "";
    //Enter details of your database
    const DB_SERVER = "YOURSERVER";
    const DB_USER = "YOURDNUSER";
    const DB_PASSWORD = "YOURDBPASSWORD";
    const DB = "YOURDB";
     
    private $db = NULL;
 
    public function __construct(){
        parent::__construct();              // Init parent contructor
        $this->dbConnect();                 // Initiate Database connection
}
     
private function dbConnect(){
        $this->db = mysqli_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
        if($this->db)
            mysqli_select_db($this->db,self::DB);
}
     
    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string
     *
     */
public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
        if((int)method_exists($this,$func) > 0)
            $this->$func();
        else
            $this->response('Error code 404, Page not found',404);   // If the method not exist with in this class, response would be "Page not found".
}

//simple get request from db using mysqli
private function getUser() {
    // Cross validation if the request method is GET else it will return "Not Acceptable" status
    if($this->get_request_method() != "GET"){
        $this->response('',406);
    }

    if(!empty($this->_request['id'])) {

        $query = "SELECT * FROM user";
        $query .= "WHERE id='".$this->_request['id']."'";

        $result = mysqli_query($this->db, $query);

        if (mysqli_num_rows($result) != 0) {
            //chg the data into json
            $this->response($this->json(mysqli_fetch_all($result)), 200);
        } else {
            //no data exist
            $this->response('', 404);
        }
        
    } else {
        $this->response('', 406);
    }

    mysqli_close($this->db);
}

//simple to update data
private function uptUser() {

    // Cross validation if the request method is GET else it will return "Not Acceptable" status
    if($this->get_request_method() != "PUT"){
        $this->response('',406);
    }

    if(!empty($this->_request['id'])) {

        $curr_datetime = date("Y-m-d G:i:s");
        $query="UPDATE user SET name_usr='".$this->_request['name']."', updated_datetime='".$curr_datetime."' WHERE id=".$this->_request['id'];

        $result = mysqli_query($this->db, $query);

        if ($result) {
            //chg the data into json
            $this->response('User updated successfully.', 200);
        } else {
            //no data exist
            $this->response('', 406);
        }
        
    } else {
        $this->response('', 406);
    }

    mysqli_close($this->db);
}
 
private function test(){    
    // Cross validation if the request method is GET else it will return "Not Acceptable" status
    if($this->get_request_method() != "GET"){
        $this->response('',406);
    }
    $myDatabase= $this->db;// variable to access your database
    $param=$this->_request['var'];
    // If success everythig is good send header as "OK" return param
       
    $this->json($this->response($param, 200));
}
 
     
    /*
     *  Encode array into JSON
    */
    private function json($data){
        if(is_array($data)){
            return json_encode($data);
        }
    }
}
 
    // Initiiate Library
     
    $api = new API;
    $api->processApi();
?>